//
//  CircularImageView.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 17/1/2019.
//  Copyright © 2019 iOS Developer Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CircularImageView: UIImageView {
    
    static let LABEL_TAG = 1007
    static let INDICATOR_TAG = 1009
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.tag = CircularImageView.LABEL_TAG
        
        return label
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .gray)
        indicator.startAnimating()
        indicator.hidesWhenStopped = true
        indicator.tag = CircularImageView.INDICATOR_TAG
        
        return indicator
    }()
    
    let disposeBag = DisposeBag()
    
    // MARK: - View Lifecycles
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.addSubview(activityIndicator)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupCircularImageLayer()
        centerActivityIndicator()
    }
    
    // MARK: - Private Functions
    private func setupCircularImageLayer() {
        let radius: CGFloat = self.bounds.size.width / 2.0
        
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFill
    }
    
    private func centerActivityIndicator() {
        activityIndicator.center = CGPoint.init(x: self.bounds.width / 2,
                                                y: self.bounds.height / 2)
    }
    
    private func addName(withLabel text: String) {
        nameLabel.text = text
        self.addSubview(nameLabel)
        nameLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    private func removeNameLabelIfExist() {
        if let subLabel: UILabel = self.viewWithTag(CircularImageView.LABEL_TAG) as? UILabel {
            subLabel.removeFromSuperview()
        }
    }
    
    // MARK: - Public Functions
    public func setBanner(withCity city: City) {
        guard let bannerUrl = city.banner, let url = URL.init(string: bannerUrl) else {
            // Banner Not Available
            
            self.backgroundColor = UIColor.init(hexString: city.color)
            addName(withLabel: String(city.name.prefix(3)))
            
            return
        }
        
        let urlRequest = URLRequest.init(url: url)
        
        activityIndicator.startAnimating()
        URLSession.shared.rx
            .response(request: urlRequest)
            .subscribeOn(MainScheduler.instance)
            .subscribe(
                onNext: { [weak self] response in
                    // Update Image
                    DispatchQueue.main.async {
                        self?.image = UIImage(data: response.data)
                        self?.activityIndicator.stopAnimating()
                    }
                }, onError: { error in
                    debugPrint(error.localizedDescription)
            }, onCompleted: {
            }).disposed(by: disposeBag)
    }
    
    public func resetImageView() {
        removeNameLabelIfExist()
        
        self.image = nil
        self.backgroundColor = UIColor.clear
    }
}
