//
//  CountryTableViewCell.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 17/1/2019.
//  Copyright © 2019 iOS Developer Inc. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    
    static let identifier = "CountryCell"
    
    @IBOutlet weak var banner: CircularImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    var data: City? {
        didSet {
            configureCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        banner.resetImageView()
    }
    
    private func configureCell() {
        guard let city = data else { return }
        
        self.title.text = city.name
        self.subTitle.text = city.subtitle
        
        banner.setBanner(withCity: city)
    }
    
}
