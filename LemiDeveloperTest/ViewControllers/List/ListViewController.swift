//
//  ListViewController.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 17/1/2019.
//  Copyright © 2019 iOS Developer Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ListViewController: UIViewController, ListViewTableViewControllerDelegate {
    @IBOutlet weak var listViewTableView: UITableView!
    private let listViewTableViewController = ListViewTableViewController()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var listViewModel = ListViewModel()
    var disposeBag = DisposeBag()
    
    weak var delegate: ListViewTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeListViewTableViewController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        _ = listViewModel.data
            .asObservable().bind(to: self.listViewTableView.rx.items) { tableView, row, element in
                let indexPath = IndexPath.init(row: row, section: 0)
                let cell = self.listViewTableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.identifier,
                                                                      for: indexPath) as! CountryTableViewCell
                cell.data = element
                
                return cell
        }.disposed(by: disposeBag)
        
        searchBar.rx.text
            .orEmpty
            .bind(to: listViewModel.searchText)
            .disposed(by: disposeBag)
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let navController = self.navigationController as? NavigationController else { return }
        navController.setNavigationBarVisiblity(false, animated: true)
    }
    
    private func initializeListViewTableViewController() {
        addChild(listViewTableViewController)
        self.listViewTableViewController.view = listViewTableView
        listViewTableView.delegate = listViewTableViewController
        listViewTableView.dataSource = listViewTableViewController
        self.listViewTableViewController.viewDidLoad()
        self.listViewTableViewController.delegate = self
    }
    
    func scrollViewWillBeginDragging() {
        searchBar.endEditing(true)
    }
    
    func didSelectRow(withCity city: City) {
        searchBar.endEditing(true)
        
        guard let navController = self.navigationController as? NavigationController else { return }
        
        delegate?.didSelectRow(withCity: city)
        navController.popViewController(animated: true)
    }
}
