//
//  ListViewTableViewController.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 17/1/2019.
//  Copyright © 2019 iOS Developer Inc. All rights reserved.
//

import UIKit

class ListViewTableViewController: UITableViewController {
    
    weak var delegate: ListViewTableViewControllerDelegate?
    
    // MARK: - View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib.init(nibName: "CountryTableViewCell", bundle: nil),
                                forCellReuseIdentifier: CountryTableViewCell.identifier)
        
        // MARK: - Let RxSwift handle the DataSourcing
        self.tableView.dataSource = nil
    }
}

extension ListViewTableViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CountryTableViewCell
        
        if let city = cell.data {
            delegate?.didSelectRow(withCity: city)
        }
    }
}

extension ListViewTableViewController {
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        delegate?.scrollViewWillBeginDragging()
    }
}
