//
//  NavigationHelper.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 17/1/2019.
//  Copyright © 2019 iOS Developer Inc. All rights reserved.
//

import UIKit

class NavigationHelper {
    static func appDelegate() -> AppDelegate? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate
    }
    
    static func loadMainScreen() {
        let mainViewController = MainViewController.init(nibName: "MainViewController",
                                                         bundle: nil)
        let navController = NavigationController.init(rootViewController: mainViewController)
        
        appDelegate()?.window?.rootViewController = navController
    }
    
    static func pushListScreen(mainView: MainViewController, navigationController: NavigationController) {
        let listViewController = ListViewController.init(nibName: "ListViewController",
                                                         bundle: nil)
        listViewController.delegate = mainView.self
        navigationController.pushViewController(listViewController, animated: true)
    }
}
