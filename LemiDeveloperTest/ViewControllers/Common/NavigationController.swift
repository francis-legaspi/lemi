//
//  NavigationController.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 17/1/2019.
//  Copyright © 2019 iOS Developer Inc. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarHidden(true, animated: false)
    }
    
    public func setNavigationBarVisiblity(_ hidden: Bool, animated: Bool) {
        setNavigationBarHidden(hidden, animated: animated)
    }
}
