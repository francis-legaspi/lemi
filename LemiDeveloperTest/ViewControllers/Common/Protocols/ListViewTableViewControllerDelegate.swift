//
//  ListViewTableViewControllerDelegate.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 18/1/2019.
//  Copyright © 2019 Hallohallo Entertainment Inc. All rights reserved.
//

import Foundation

protocol ListViewTableViewControllerDelegate: class {
    func didSelectRow(withCity city: City)
    func scrollViewWillBeginDragging()
}
