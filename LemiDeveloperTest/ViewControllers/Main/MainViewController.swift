//
//  MainViewController.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 17/1/2019.
//  Copyright © 2019 iOS Developer Inc. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var selectedLocation: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let navController = self.navigationController as? NavigationController else { return }
        navController.setNavigationBarVisiblity(true, animated: false)
    }
    
    // MARK: - Actions
    @IBAction func onTapToSelectCityHandler(_ sender: UIButton) {
        guard let navController = self.navigationController as? NavigationController else { return }
        
        NavigationHelper.pushListScreen(mainView: self, navigationController: navController)
    }

}

extension MainViewController: ListViewTableViewControllerDelegate {
    func didSelectRow(withCity city: City) {
        debugPrint(city)
        
        selectedLocation.text = "You selected: \(city.name)"
    }
    
    func scrollViewWillBeginDragging() {}
}
