//
//  City.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 17/1/2019.
//  Copyright © 2019 iOS Developer Inc. All rights reserved.
//

import Foundation

struct City: Decodable {
    let name: String
    let subtitle: String
    let countrycode: String
    let population: Double?
    let longitude: Double?
    let latitude: Double?
    let type: String
    let id: String
    let zoom: Int?
    let banner: String?
    let color: String
    
    enum CityDataCodingKeys: String, CodingKey {
        case name
        case subtitle
        case countryCode = "country_code"
        case population
        case longitude
        case latitude
        case type
        case id
        case zoom
        case banner
        case color
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CityDataCodingKeys.self)
        
        self.name = try container.decode(String.self, forKey: .name)
        self.subtitle = try container.decode(String.self, forKey: .subtitle)
        self.countrycode = try container.decode(String.self, forKey: .countryCode)
        self.population = try? container.decode(Double.self, forKey: .population)
        self.longitude = try? container.decode(Double.self, forKey: .longitude)
        self.latitude = try? container.decode(Double.self, forKey: .latitude)
        self.type = try container.decode(String.self, forKey: .type)
        self.id = try container.decode(String.self, forKey: .id)
        self.zoom = try? container.decode(Int.self, forKey: .zoom)
        self.banner = try? container.decode(String.self, forKey: .banner)
        self.color = try container.decode(String.self, forKey: .color)
    }
}
