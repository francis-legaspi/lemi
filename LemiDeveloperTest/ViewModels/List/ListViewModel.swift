//
//  ListViewModel.swift
//  LemiDeveloperTest
//
//  Created by Francis Joshua Legaspi on 17/1/2019.
//  Copyright © 2019 iOS Developer Inc. All rights reserved.
//

import RxSwift
import RxCocoa

class ListViewModel {
    
    let searchText: BehaviorRelay<String> = BehaviorRelay<String>(value: "")

    lazy var data: Driver<[City]> = {
        return self.searchText.asObservable()
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .flatMapLatest(ListViewModel.searchBy)
            .asDriver(onErrorJustReturn: [])
    }()
    
    static func searchBy(city: String) -> Observable<[City]> {
        guard let url = URL.init(string: "https://lemi.travel/api/v5/cities?q=\(city)") else {
            return Observable.just([])
        }
        
        return URLSession.shared.rx.json(url: url)
                .retry(3)
                .catchErrorJustReturn([])
                .map(parse)
    }
    
    static func parse(json: Any) -> [City] {
        guard let jsonData = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted) else {
            return []
        }
        
        guard let city = try? JSONDecoder().decode([City].self, from: jsonData) else {
            return []
        }
        
        return city
    }
}
