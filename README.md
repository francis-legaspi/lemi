# lemi

### Installation

This App requires [RxSwift](https://github.com/ReactiveX/RxSwift) to run.

To install the depedencies just run **pod install** in the project's root folder.

```sh
$:lemi developer$ pod install
```